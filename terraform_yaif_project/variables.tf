variable "env_variable" {
  description = "example variable, passed through env var"
  type        = string
}

variable "tfvars_variable" {
  description = "Other example variable, passed through tfvars"
  type        = string
}
