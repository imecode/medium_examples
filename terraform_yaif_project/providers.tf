terraform {
  required_providers {}
  required_version = ">= 1.1.0, < 2.0.0"

  backend "local" {}
}
