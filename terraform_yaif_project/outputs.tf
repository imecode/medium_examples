output "output_value" {
  value = jsonencode({
    var1 = var.env_variable
    var2 = var.tfvars_variable
  })
}