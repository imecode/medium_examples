variable "env" {
  description = "Environment name, passed through env var"
  type        = string
}

variable "output_file_name" {
  description = "Output file name, passed through tfvars"
  type        = string
}
