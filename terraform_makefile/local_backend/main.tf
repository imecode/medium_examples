resource "local_file" "file_env" {
  content = jsonencode({
    env              = var.env
    output_file_name = var.output_file_name
  })
  filename = "${path.module}/${var.output_file_name}"
}